import os, sys, site
import subprocess
import setuptools

requires = [ 'numpy' ]

setuptools.setup(
    name="ilos",
    version="0.1.0",
    description="ilos (In Lieu Of a Sensor) is a screen-brightness manager that uses the camera to manage a healthy level of backlight intensity.",
    author="Ian Ressa",
    author_email="ian@eonndev.com",
    license='GPLv3',
    url="https://gitlab.com/iressa/ilos",
    install_requires = requires
)
